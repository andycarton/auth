package auth

import (
	"crypto/cipher"
	"errors"
	"strconv"
)

// The AES block size in bytes.
//const BlockSize = 64

// A cipher is an instance of AES encryption using a particular key.
type aesCipher struct {
	enc []uint32
	dec []uint32
	bs  int
}

func KeySizeError(k, s int) error {
	return errors.New("auth: invalid key size " + strconv.Itoa(k) + ", block size " + strconv.Itoa(s))
}

// NewCipher creates and returns a new cipher.Block.
// The key argument should be the AES key,
// either 16, 24, or 32 bytes to select
// AES-128, AES-192, or AES-256.
func NewCipher(key []byte, blockSize int) (cipher.Block, error) {
	k := len(key)
	switch k {
	default:
		return nil, KeySizeError(k, blockSize)
	case 16, 24, 32:
		break
	}
	return newCipherGeneric(key, blockSize)
}

// newCipherGeneric creates and returns a new cipher.Block
// implemented in pure Go.
func newCipherGeneric(key []byte, blockSize int) (cipher.Block, error) {
	n := len(key) + 28
	c := aesCipher{enc: make([]uint32, n), dec: make([]uint32, n), bs: blockSize}
	expandKeyGo(key, c.enc, c.dec)
	return &c, nil
}

func (c *aesCipher) BlockSize() int { return c.bs }

func (c *aesCipher) Encrypt(dst, src []byte) {
	if len(src) < c.bs {
		panic("crypto/aes: input not full block")
	}
	if len(dst) < c.bs {
		panic("crypto/aes: output not full block")
	}
	if InexactOverlap(dst[:c.bs], src[:c.bs]) {
		panic("crypto/aes: invalid buffer overlap")
	}
	encryptBlockGo(c.enc, dst, src)
}

func (c *aesCipher) Decrypt(dst, src []byte) {
	if len(src) < c.bs {
		panic("crypto/aes: input not full block")
	}
	if len(dst) < c.bs {
		panic("crypto/aes: output not full block")
	}
	if InexactOverlap(dst[:c.bs], src[:c.bs]) {
		panic("crypto/aes: invalid buffer overlap")
	}
	decryptBlockGo(c.dec, dst, src)
}
