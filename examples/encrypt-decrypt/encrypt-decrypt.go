package main

import (
	"fmt"
	"strconv"
	"time"

	"gitlab.com/andycarton/auth"
)

var key = "1234567890123456"
var vKey = "12345678901234566543210987654321"

type Cryptor interface {
	Encode(data []byte) ([]byte, error)
	Decode(data []byte) ([]byte, error)
}

func main() {
	crypt, err := auth.NewCrypt(key, vKey, false)
	if err != nil {
		fmt.Println(err)
		return
	}

	expires := time.Now().Add(1 * time.Hour).Unix()
	data := []byte(`{"expires":` + strconv.FormatInt(expires, 10) + `}`)

	token, err := encrypt(crypt, data)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(token))

	data, err = decrypt(crypt, token)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(data))

}

func encrypt(c Cryptor, data []byte) ([]byte, error) {
	data, err := c.Encode(data)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func decrypt(c Cryptor, data []byte) ([]byte, error) {
	data, err := c.Decode(data)
	if err != nil {
		return nil, err
	}
	return data, nil
}
