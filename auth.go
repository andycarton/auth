package auth

import (
	"bytes"
	"crypto/cipher"
	"encoding/hex"
	"encoding/json"
	"errors"
	"strconv"
)

var ErrEmptyModel = errors.New("auth: empty model")

// decryptor .
type decryptor struct {
	vKey  []byte
	block cipher.Block
}

// NewCrypt .
func NewCrypt(key, vKey string, encoded bool) (*decryptor, error) {

	var err error

	encryptKey := []byte(key)
	validateKey := []byte(vKey)

	switch encoded {
	case false:
		encryptKey = []byte(hex.EncodeToString(s2b(key)))
		validateKey = []byte(hex.EncodeToString(s2b(vKey)))
	default:
		encryptKey, err = hex.DecodeString(key)
		if err != nil {
			return nil, err
		}

		validateKey, err = hex.DecodeString(vKey)
		if err != nil {
			return nil, err
		}
	}

	block, err := NewCipher(encryptKey, len(validateKey))
	if err != nil {
		return nil, err
	}

	d := &decryptor{vKey: validateKey, block: block}
	return d, nil
}

func (d *decryptor) Encode(data []byte) ([]byte, error) {

	if len(data)%d.block.BlockSize() != 0 {
		data = append(data, bytes.Repeat([]byte{0}, d.block.BlockSize()-len(data)%d.block.BlockSize())...)
	}

	ciphertext := make([]byte, len(data))

	if len(d.vKey) != d.block.BlockSize() {
		return nil, ErrorNotEqBlockSize(len(d.vKey), d.block.BlockSize())
	}

	mode := cipher.NewCBCEncrypter(d.block, d.vKey)
	mode.CryptBlocks(ciphertext, data)
	dst := s2b(hex.EncodeToString(ciphertext))

	return dst, nil
}

func (d *decryptor) EncodeJSON(mod interface{}) ([]byte, error) {

	blob, err := json.Marshal(mod)
	if err != nil {
		return nil, err
	}
	return d.Encode(blob)
}

func (d *decryptor) Decode(token []byte) ([]byte, error) {
	cipherTextDecoded, err := hex.DecodeString(b2s(token))
	if err != nil {
		return nil, err
	}

	mode := cipher.NewCBCDecrypter(d.block, d.vKey)

	if len(cipherTextDecoded)%d.block.BlockSize() != 0 {
		cipherTextDecoded = append(cipherTextDecoded, bytes.Repeat([]byte{0}, d.block.BlockSize()-len(cipherTextDecoded))...)
	}

	mode.CryptBlocks(cipherTextDecoded, cipherTextDecoded)

	if i := bytes.Index(cipherTextDecoded, []byte{0}); i >= 0 {
		cipherTextDecoded = cipherTextDecoded[:i]
	}
	return cipherTextDecoded, nil
}

func (d *decryptor) DecodeJSON(token []byte, mod interface{}) error {
	if mod == nil {
		return ErrEmptyModel
	}

	blob, err := d.Decode(token)
	if err != nil {
		return err
	}

	return json.Unmarshal(blob, mod)
}

func ErrorNotEqBlockSize(vkeyLen, blockSize int) error {
	return errors.New("validate key (" + strconv.Itoa(vkeyLen) + ") length must equal block size (" + strconv.Itoa(blockSize) + ")")
}
