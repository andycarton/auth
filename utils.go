package auth

import (
	"reflect"
	"unsafe"
)

// b2s convertation with zero alloc
func b2s(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

// s2b - string to bytes
func s2b(s string) []byte {
	h := (*reflect.StringHeader)(unsafe.Pointer(&s))
	return *(*[]byte)(unsafe.Pointer(&reflect.SliceHeader{Len: h.Len, Cap: h.Len, Data: h.Data}))
}
